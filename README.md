## Template Gradle Scripts.

Remote scripts to make batch maintenance easier.

### How to use:

```
> declare needed variables/properties in gradle.properties <
plugins {
    [required plugins for scripts]
}

apply from: 'https://gitlab.com/advanced-power-opensource/gradle/-/raw/master/basics.gradle'
apply from: 'https://gitlab.com/advanced-power-opensource/gradle/-/raw/master/maven-publish.gradle'
apply from: 'https://gitlab.com/advanced-power-opensource/gradle/-/raw/master/ap-publish.gradle'
apply from: 'https://gitlab.com/advanced-power-opensource/gradle/-/raw/master/spring-deploy.gradle'

...
```

### How to debug:

It should show up with relevant error messages when the script is broken and such. Otherwise,
you can download the scripts and apply them by path. This allows for easier editing, etc.

### How?

https://docs.gradle.org/current/userguide/plugins.html#sec:script_plugins